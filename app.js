let c = document.getElementById('myCanvas');
let ctx = c.getContext("2d");

// X axes
ctx.beginPath();
ctx.moveTo(100, 0);
ctx.lineTo(100, 200);
ctx.stroke();

// Y axes
ctx.beginPath();
ctx.moveTo(0, 100);
ctx.lineTo(200, 100);
ctx.stroke();

// Return 3x + 5
const equation = x => 3*x + 5;

// Start Values 
startX = 100;
startY = 100;

ctx.beginPath();
ctx.moveTo(startX, startY);
for(let i = -100 ; i<100; i++){
    ctx.lineTo(startX + i , startY - (equation(i)))
}

ctx.strokeStyle = 'red';
ctx.stroke();